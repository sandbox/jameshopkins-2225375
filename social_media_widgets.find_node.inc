<?php

/**
 * @file
 */

 /**
  * Adds autocomplete functionality to make finding nodes easier.
  */
function _social_media_widgets_find_node($string = "") {
  $matches = array();
  if ($string) {
    $result = db_select('node');
    $result->fields('node', array('nid', 'title', 'type'));
    $result->condition('title', db_like($string) . '%', 'LIKE');
    $result->condition('language', array($GLOBALS['language']->language, LANGUAGE_NONE), 'IN');
    $disallowed_node_types = variable_get('social_media_widgets_disallowed_node_types');
    if (!empty($disallowed_node_types)) {
      $result->condition('type', $disallowed_node_types, 'NOT IN');
    }
    $result->range(0, 10);
    $output = $result->execute();
    foreach ($output as $node) {
      $matches[$node->title . " [$node->nid]"] = $node->title . " [$node->nid $node->type]";
    }
  }
  drupal_json_output($matches);
}
