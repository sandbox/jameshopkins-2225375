DESCRIPTION:
-------------------
This module allows the configuration - and subsequent embedding of - one or
more HTML/JS-based social media APIs.

INSTALLATION & CONFIGURATION:
-------------------
1. Enable the module
2. Define one or more API definitions by implementing
   hook_social_widget_schemas().
3. Assign the block 'social_media_widgets' to a visible region on your site.
4. Go to 'admin/config/system/social-media-widgets' and create one or more
   widget instances, based upon the API definition(s) you created previously.
