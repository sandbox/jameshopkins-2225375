<?php

/**
 * @file
 * Defines the admin-specific form necessary to create, edit, and
 * delete social widget definitions.
 */

/**
 * Page callback for the main social widgets landing page.
 */
function _social_media_widgets_overview() {
  if (drupal_multilingual()) {
    $output['language_switcher'] = array(
      '#theme' => 'links',
      '#links' => language_negotiation_get_switch_links('language', $_GET['q'])->links,
      '#attributes' => array('class' => array('language_switcher')),
      '#prefix' => t('This is the %language specific configuration page. You can select another language version below:', array('%language' => $GLOBALS['language']->native)),
    );
  }
  // Render the 'Create feeds' buttons
  $platforms = social_media_widgets_widget_schema_load();
  foreach ($platforms as $name => &$platform) {
    $platform = array(
      'title' => t('Add @platform feed', array('@platform' => $platform['platform'])),
      'href' => 'admin/config/system/social-media-widgets/add/' . $name,
    );
  }
  unset($platform);
  $output['add_instance'] = array(
    '#theme' => 'links',
    '#links' => $platforms,
    '#attributes' => array('class' => array('action-links')),
  );

  // Render the summary table
  $nids = _social_media_widgets_get_unique_pages();
  $rows = array();
  foreach ($nids as $nid) {
    $page = drupal_get_path_alias('node/' . $nid);
    $node = node_load($nid);
    $rows[$nid] = array(
      '<h3>' . $node->title . '</h3>' .
      l($page, $page, array('attributes' => array('target' => '_blank'))),
      array(
        'data' => render(drupal_get_form('social_media_widgets_instances_per_page', $nid)),
        'class' => 'page-instances'),
    );
  }
  $output['instances_list'] = array(
    '#theme' => 'table',
    '#header' => array('page' => t('Page'), 'feeds' => t('Feeds')),
    '#rows' => $rows,
    '#empty' => t('No social widget instances for your current language exist yet.'),
    '#attributes' => array('id' => 'social-widget-overview'),
  );
  
  return $output;
}

/**
 * Form constructor to list all the social widget instances on each page.
 *
 * @see social_media_widgets_instances_per_page_submit()
 */
function social_media_widgets_instances_per_page($form, &$form_state, $nid) {
  $instances = _social_media_widgets_get_widget_instances($nid);
  $form['instances_by_page'] = array(
    '#theme' => 'social_media_widgets_instances_by_page',
    '#tree' => TRUE,
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'social_media_widgets') . '/css/instances-table.css'),
    ),
  );
  
  foreach ($instances as $index => $instance) {
    $parameters = drupal_json_decode($instance['parameters']);
    $form['instances_by_page'][$index]['parameters'] = array(
      '#theme' => 'social_media_widgets_widget_instance_parameters',
      '#parameters' => $parameters,
    );
    $form['instances_by_page'][$index]['platform'] = array(
      '#type' => 'item',
      '#markup' => $instance['platform'],
    );
    $form['instances_by_page'][$index]['edit'] = array(
      '#markup' => l(t('Edit'), 'admin/config/system/social-media-widgets/edit/' . $instance['sid']),
    );
    $form['instances_by_page'][$index]['delete'] = array(
      '#markup' => l(t('Delete'), 'admin/config/system/social-media-widgets/delete/' . $instance['sid']),
    );
    $form['instances_by_page'][$index]['weight'] = array(
      '#type' => 'weight',
      //'#default_value' => $instance['weight'],
      '#delta' => 10,
      '#attributes' => array('class' => array('row-weight')),
    );
    $form['instances_by_page'][$index]['sid'] = array(
      '#type' => 'hidden',
      '#value' => $instance['sid'],    
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save order'),
    '#attributes' => array('class' => array('save-order')),
  );
  return $form;
}

/**
 * Form submission handler for social_media_widgets_instances_per_page().
 */
function social_media_widgets_instances_per_page_submit($form, &$form_state) {
  $rows = $form_state['values']['instances_by_page'];
    
  foreach ($rows as $row) {
    $update = db_update('social_media_widgets')
      ->fields(array(
        'weight' => $row['weight'],
      ))
      ->condition('sid', $row['sid'], '=')
      ->execute();
  }
  drupal_set_message(t('Feed instance ordering has been updated.'));
}

/**
 * Modifies the base instance form schema, and adds elements necessary
 * for creating feed instances.
 * 
 * @see social_media_widgets_instance_base_form_schema()
 * @see social_media_widgets_validate_autocomplete()
 */
function social_media_widgets_load_instance_schema_add(&$form) {
  $form['nid']['#element_validate'] = array('social_media_widgets_validate_autocomplete');
  $form['nid']['#autocomplete_path'] = 'admin/config/system/social-media-widgets/find-node';
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create feed'),
  );
}

/**
 * Modifies the base instance form schema, and adds elements necessary
 * for editing feed instances.
 * 
 * @see social_media_widgets_instance_base_form_schema()
 */
function social_media_widgets_load_instance_schema_edit(&$form, $sid) {
  $instance = _social_media_widgets_load_instance_by_sid($sid);
  $form['nid']['#value'] = drupal_get_path_alias('node/' . $instance['nid']);
  $form['nid']['#disabled'] = TRUE;
  $form['platform'] = array(
    '#type' => 'hidden',
    '#value' => $instance['platform'],
  );
  $form['sid'] = array(
    '#type' => 'hidden',
    '#value' => $instance['sid'],
  );
  $form['nid_raw'] = array(
    '#type' => 'hidden',
    '#value' => $instance['nid'],
  );
  
  foreach (drupal_json_decode($instance['parameters']) as $name => $parameter) {
    $form['parameters'][$name]['#default_value'] = $parameter;
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save feed'),
  );
}

/**
 * Define a standard schema that's shared across both 'add' and 'edit'
 * instances of the form.
 * 
 * @see social_media_widgets_load_instance_schema_add()
 * @see social_media_widgets_load_instance_schema_edit()
 */
function social_media_widgets_instance_base_form_schema($platform_name, $sid = '') {

  $platforms = social_media_widgets_widget_schema_load();
  $form['nid'] = array(
    '#type' => 'textfield',
    '#title' => 'Page',
    '#description' => t('The page in which to embed the feed instance on.'),
    '#required' => TRUE,
  );
  $form['parameters']['height'] = array(
    '#title' => t('Height'),
    '#description' => t('
    Be conscious that the height of the widget has the potential to increase exponentially,
    based on how many widget options you enable. This means you could be left with a large
    gap below a particularly tall widget, whose contents are relatively limited.
    '),
    '#type' => 'select',
    '#options' => array(
      '200px' => t('Short'),
      '400px' => t('Tall'),
    ),
  );
  foreach ($platforms[$platform_name]['parameters'] as $name => $parameter) {
    $form['parameters'][$name] = $parameter;
  }
  
  if (empty($sid)) {
    // Add the 'add instance' schema to the form
    social_media_widgets_load_instance_schema_add($form); 
  }
  else {
    // Add the 'edit instance' schema to the form
    social_media_widgets_load_instance_schema_edit($form, $sid);
  }
  
  $form['#tree'] = TRUE;

  return $form;
}

/**
 * Form constructor that allows for the editing of an existing social widget
 * instance.
 *
 * @see social_media_widgets_instance_base_form_schema()
 * @see social_media_widgets_instance_edit_form_submit()
 */
function social_media_widgets_instance_edit_form($form, &$form_state, $sid) {
  $instance = _social_media_widgets_load_instance_by_sid($sid);
  if (empty($instance)) {
    drupal_set_message(t('Unable to edit this feed instance, as an invalid SID was given.'), 'error');
  }
  else {
    $form = social_media_widgets_instance_base_form_schema($instance['platform'], $instance['sid']);
    return $form;
  }
}

/**
 * Form submission handler for social_media_widgets_instance_edit_form().
 */
function social_media_widgets_instance_edit_form_submit($form, &$form_state) {
  $record = array(
    'parameters' => drupal_json_encode($form_state['values']['parameters']),
    'language' => $GLOBALS['language']->language,
    'nid' => $form_state['values']['nid_raw'],
    'platform' => $form_state['values']['platform'],
    'sid' => $form_state['values']['sid'],
  );
  drupal_write_record('social_media_widgets', $record, 'sid');
  drupal_set_message(t('The feed instance has been updated'));
  $form_state['redirect'] = 'admin/config/system/social-media-widgets';
}

/**
 * Form constructor that allows for the addition of social widget instances.
 *
 * @see social_media_widgets_instance_base_form_schema()
 * @see social_media_widgets_instance_add_form_submit()
 */
function social_media_widgets_instance_add_form($form, &$form_state, $platform_name) {
  $platforms = social_media_widgets_widget_schema_load();
  if (!array_key_exists($platform_name, $platforms)) {
    drupal_set_message(t('You are trying to add a feed instance based on a platform that hasn\'t yet been defined (using <code>hook_social_widget_platforms()</code>).'), 'error');
  }
  else {
    drupal_set_title(t('Add @platform feed', array('@platform' => $platforms[$platform_name]['platform'])));
    $form = social_media_widgets_instance_base_form_schema($platform_name);
    return $form;
  }
}

/**
 * Form submission handler for social_media_widgets_instance_add_form().
 */
function social_media_widgets_instance_add_form_submit($form, &$form_state) {
  $record = array(
    'parameters' => drupal_json_encode($form_state['values']['parameters']),
    'language' => $GLOBALS['language']->language,
    'nid' => $form_state['values']['nid'],
    'platform' => arg(5),
  );
  drupal_write_record('social_media_widgets', $record);
  drupal_set_message(t('A new feed set to appear on %page has been added', array(
    '%page' => drupal_get_path_alias('node/' . $form_state['values']['nid']),
  )));
  $form_state['redirect'] = 'admin/config/system/social-media-widgets';
}

/**
 * Form validation handler that validates the node ID inserted into the form
 * through the autocomplete functionality.
 */
function social_media_widgets_validate_autocomplete($element, &$form_state) {
  $matches = array();
  $nid = 0;
  // This preg_match() looks for the last pattern like [33334] and if found
  // extracts the numeric portion.
  $result = preg_match('/\[([0-9]+)\]$/', $form_state['values']['nid'], $matches);
  if ($result > 0) {
    // If $result is nonzero, we found a match and can use it as the index
    // into $matches.
    $nid = $matches[$result];
    // Verify that it's a valid nid.
    $node = node_load($nid);
    if (!empty($node)) {
      $form_state['values']['nid'] = $nid;
    }
    else {
      form_error($form['nid'], t('Sorry, no node with a node ID of %nid can be found', array('%nid' => $nid)));
      return;
    }
  }
}

/**
 * Form constructor that allows for the deletion of a social widget instance.
 *
 * @param $instance string
 *   The ID of the social widget instance to be deleted.
 *
 * @see social_media_widgets_instance_delete_submit()
 */
function social_media_widgets_instance_delete($form, &$form_state, $instance) {
  $form['sid'] = array('#type' => 'hidden', '#value' => $instance);
  return confirm_form($form, t('Are you sure you want to delete this particular social widget instance?'), 'admin/config/system/social-media-widgets', '', t('Delete'), t('Cancel'));
}

/**
 * Form submission handler for social_media_widgets_instance_delete().
 */
function social_media_widgets_instance_delete_submit($form, &$form_state) {
  $deleted = db_delete('social_media_widgets')
  ->condition('sid', $form_state['values']['sid'])
  ->execute();
  drupal_set_message(t('The social widget instance has been successfully deleted.'));
  $form_state['redirect'] = 'admin/config/system/social-media-widgets';
}

