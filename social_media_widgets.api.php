<?php

/**
 * @file
 * Hooks provided by Social Media.
 */

/**
 * Social Media API hook that allows modules to define social widget
 * schemas.
 *
 * Each schema definition is parsed directly into several admin-based
 * forms, so it needs to comprise of a valid renderable array as per
 * https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html
 *
 * It's important to note that each $schemas item defines it's own $parameters
 * array, with each individual parameter mapping directly to a parameter
 * placeholder within it's own HTML/JS widget embed code
 * - i.e 'username' => {USERNAME}, 'tweet_limit' => {TWEET_LIMIT}
 */
function hook_social_widget_schemas() {
  $schemas = array();
  $schemas['twitter-username'] = array(
    'platform' => t('Twitter (Username)'),
    'parameters' => array(
      'username' => array(
        '#title' => t('Username'),
        '#type' => 'textfield',
        '#required' => TRUE,
      ),
      'widget_id' => array(
        '#title' => t('Widget ID'),
        '#type' => 'textfield',
        '#description' => '',
        '#required' => TRUE,
      ),
      'chrome' => array(
        '#title' => t('Chrome'),
        '#type' => 'textfield',
        '#description' => t('Controls the widget layout and chrome. Use a space-seperated list from the <a href="https://dev.twitter.com/docs/embedded-timelines#options">defined options</a>.'),
      ),
      'tweet_limit' => array(
        '#title' => t('Tweet limit'),
        '#type' => 'textfield',
      ),
    ),
    'embed' => <<<EOD
    <a class="twitter-timeline"  href="https://twitter.com/{USERNAME}" data-tweet-limit="{TWEET_LIMIT}" data-chrome="{CHROME}" height="400" data-widget-id="{WIDGET_ID}">Tweets by @{USERNAME}</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
EOD
  );
  return $schemas;
};
